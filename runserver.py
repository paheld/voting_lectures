#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import sys
import os
import logging
#logging.basicConfig(level=logging.DEBUG)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "voting_lectures.settings")

import daemon


try:
    import pymysql
    pymysql.install_as_MySQLdb()
except ImportError:
    pass

import django
django.setup()

import django.core.handlers.wsgi

import tornado.httpserver
import tornado.ioloop
import tornado.wsgi
import tornado.web
import tornado.websocket
import json
from threading import RLock, Thread

import voting_lectures.settings as settings


def run():
    wsgi_app = tornado.wsgi.WSGIContainer(django.core.handlers.wsgi.WSGIHandler())
    tornado_app = tornado.web.Application([
        (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': settings.STATIC_ROOT}),
        ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),
      ])
    
    server = tornado.httpserver.HTTPServer(tornado_app)

    server.listen(8001)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    if len(sys.argv) == 2:
        d = daemon.Daemon(os.path.dirname(os.path.abspath(sys.argv[0]))+"/run.pid", run)
        daemon.deamon_manager(d)
    else:
        run()
