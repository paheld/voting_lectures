from django.conf.urls import include, url
from django.contrib import admin

import webapp.views

urlpatterns = [
    # Examples:
    # url(r'^$', 'voting_lectures.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', webapp.views.index, name="index"),
    url(r'^(?P<lecture_id>[0-9]+)/students/$', webapp.views.get_students, name="students"),
    url(r'^(?P<lecture_id>[0-9]+)/sheets/$', webapp.views.sheets, name="sheets"),
    url(r'^(?P<lecture_id>[0-9]+)/student_list/$', webapp.views.student_list, name="student_list"),
    url(r'^(?P<lecture_id>[0-9]+)/frs_import_start/$', webapp.views.frs_import_start, name="frs_import_start"),
    url(r'^(?P<lecture_id>[0-9]+)/lecture_rights/$', webapp.views.lecture_rights, name="lecture_rights"),
    url(r'^(?P<sheet_id>[0-9]+)/voting_list/$', webapp.views.voting_list, name="voting_list"),
    url(r'^exam/(?P<exam_id>[0-9]+)/$', webapp.views.exam, name="exam"),
    url(r'^students/$', webapp.views.student_suggestions, name="student_suggestions"),
    url(r'^student-query/$', webapp.views.query_user, name="query_user"),

    url(r'^actions/add_student_to_lecture/$', webapp.views.add_student_to_lecture, name="add_student_to_lecture"),
    url(r'^actions/add_student_to_group/$', webapp.views.add_student_to_group, name="add_student_to_group"),
    url(r'^actions/remove_student/$', webapp.views.remove_student, name="remove_student"),
    url(r'^actions/update_group_assignment/$', webapp.views.update_group_assignment, name="update_group_assignment"),
    url(r'^actions/update_exam_assignment/$', webapp.views.update_exam_assignment, name="update_exam_assignment"),
    url(r'^actions/submit_comment/$', webapp.views.submit_comment, name="submit_comment"),
    url(r'^actions/get_lecture_comments/$', webapp.views.get_lecture_comments, name="get_lecture_comments"),
    url(r'^actions/create_sheet/$', webapp.views.create_sheet, name="create_sheet"),
    url(r'^actions/create_task/$', webapp.views.create_task, name="create_task"),
    url(r'^actions/update_tick_status/$', webapp.views.update_tick_status, name="update_tick_status"),
    url(r'^actions/update_exam_student_points/$', webapp.views.update_exam_student_points, name="update_exam_student_points"),
    url(r'^actions/update_exam_points/$', webapp.views.update_exam_points, name="update_exam_points"),
    url(r'^actions/update_exam_config/$', webapp.views.update_exam_config, name="update_exam_config"),
    url(r'^actions/update_marks/$', webapp.views.update_marks, name="update_marks"),
    url(r'^actions/frs_import_process/$', webapp.views.frs_import_process, name="frs_import_process"),
    url(r'^actions/change_semester/(?P<semester_id>[0-9]+)/$', webapp.views.change_semester, name="change_semester"),
    url(r'^actions/get_mail_list/lecture/(?P<lecture_id>[0-9]+)/$', webapp.views.get_mail_list, name="get_mail_list_lecture"),
    url(r'^actions/get_mail_list/group/(?P<group_id>[0-9]+)/$', webapp.views.get_mail_list, name="get_mail_list_group"),
    url(r'^actions/get_mail_list/exam/(?P<exam_id>[0-9]+)/$', webapp.views.get_mail_list, name="get_mail_list_exam"),
    url(r'^actions/create_lecture/(?P<name>[^/]+)/$', webapp.views.create_lecture, name="create_lecture"),
    url(r'^actions/create_group/(?P<lecture_id>[0-9]+)/(?P<desc>[^/]+)/$', webapp.views.create_group, name="create_group"),
    url(r'^actions/create_exam/(?P<lecture_id>[0-9]+)/(?P<datestr>[^/]+)/$', webapp.views.create_exam, name="create_exam"),
    url(r'^actions/set_lecture_rights/(?P<lecture_id>[0-9]+)/(?P<user_id>[0-9]+)/(?P<subset>[^/]+)/(?P<value>[01])/$', webapp.views.set_lecture_rights, name="set_lecture_rights"),
    url(r'^actions/check_permissions/$', webapp.views.check_permissions, name="check_permissions"),
    url(r'^export/lecture/(?P<lecture_id>[0-9]+).xlsx$', webapp.views.lecture_excel_export, name="lecture_excel_export"),
]
