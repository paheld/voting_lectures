# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0011_auto_20150716_1033'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exam',
            name='rating_points_diff',
        ),
        migrations.AddField(
            model_name='exam',
            name='rating_points_diff_ratio',
            field=models.FloatField(default=5),
        ),
    ]
