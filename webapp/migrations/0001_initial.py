# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('desc', models.CharField(max_length=64)),
            ],
            options={
                'ordering': ['lecture'],
            },
        ),
        migrations.CreateModel(
            name='Lecture',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('english', models.BooleanField(default=False)),
                ('symbol', models.CharField(max_length=8)),
                ('name', models.CharField(max_length=64)),
                ('min_voting_ratio', models.FloatField(default=0.66)),
                ('min_presentations', models.IntegerField(default=2)),
                ('number_of_tasks', models.IntegerField(default=0)),
            ],
            options={
                'ordering': ['semester', 'name'],
            },
        ),
        migrations.CreateModel(
            name='Semester',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('year', models.IntegerField()),
                ('winter', models.BooleanField()),
            ],
            options={
                'ordering': ['-year', '-winter'],
            },
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('uid', models.CharField(max_length=64, null=True)),
                ('given_name', models.CharField(max_length=64)),
                ('surname', models.CharField(max_length=64)),
                ('mail', models.EmailField(max_length=254, null=True)),
                ('custom_mail', models.EmailField(max_length=254, null=True)),
                ('matrikel_number', models.IntegerField(null=True)),
            ],
        ),
        migrations.AddField(
            model_name='lecture',
            name='semester',
            field=models.ForeignKey(to='webapp.Semester'),
        ),
        migrations.AddField(
            model_name='group',
            name='lecture',
            field=models.ForeignKey(to='webapp.Lecture'),
        ),
        migrations.AddField(
            model_name='group',
            name='students',
            field=models.ManyToManyField(blank=True, to='webapp.Student'),
        ),
    ]
