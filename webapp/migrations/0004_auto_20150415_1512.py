# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0003_lecture_students'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sheet',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('number', models.IntegerField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('number', models.IntegerField(null=True, blank=True)),
                ('label', models.CharField(null=True, max_length=16, blank=True)),
                ('optional', models.BooleanField(default=False)),
                ('sheet', models.ForeignKey(to='webapp.Sheet')),
            ],
        ),
        migrations.RemoveField(
            model_name='lecture',
            name='number_of_tasks',
        ),
        migrations.AddField(
            model_name='sheet',
            name='lecture',
            field=models.ForeignKey(to='webapp.Lecture'),
        ),
    ]
