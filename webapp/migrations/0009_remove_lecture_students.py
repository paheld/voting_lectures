# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0008_lecture_participants'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lecture',
            name='students',
        ),
    ]
