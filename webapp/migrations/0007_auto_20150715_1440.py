# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0006_auto_20150417_0907'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudentLectureMap',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('comment', models.TextField(blank=True, null=True)),
                ('admission', models.NullBooleanField(default=None)),
            ],
        ),
        migrations.AlterModelOptions(
            name='student',
            options={'ordering': ['surname', 'given_name', 'uid']},
        ),
        migrations.AlterField(
            model_name='lecture',
            name='students',
            field=models.ManyToManyField(blank=True, related_name='lecture_set_old', to='webapp.Student'),
        ),
        migrations.AddField(
            model_name='studentlecturemap',
            name='lecture',
            field=models.ForeignKey(to='webapp.Lecture'),
        ),
        migrations.AddField(
            model_name='studentlecturemap',
            name='student',
            field=models.ForeignKey(to='webapp.Student'),
        ),
    ]
