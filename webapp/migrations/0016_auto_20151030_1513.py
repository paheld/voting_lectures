# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0015_auto_20151030_1438'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='lecture',
            options={'ordering': ['semester', 'name'], 'permissions': (('handle_lecture', 'Handle lecture'), ('view_lecture', 'View lecture'))},
        ),
    ]
