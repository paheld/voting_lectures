# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0007_auto_20150715_1440'),
    ]

    operations = [
        migrations.AddField(
            model_name='lecture',
            name='participants',
            field=models.ManyToManyField(to='webapp.Student', blank=True, through='webapp.StudentLectureMap'),
        ),
    ]
