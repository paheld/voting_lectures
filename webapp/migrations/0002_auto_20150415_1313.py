# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='given_name',
            field=models.CharField(max_length=64, null=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='surname',
            field=models.CharField(max_length=64, null=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='uid',
            field=models.CharField(unique=True, max_length=64, null=True),
        ),
    ]
