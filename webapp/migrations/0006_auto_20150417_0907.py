# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0005_sheet_last_sheet'),
    ]

    operations = [
        migrations.CreateModel(
            name='TickedTask',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('presented', models.BooleanField(default=False)),
                ('student', models.ForeignKey(to='webapp.Student')),
                ('task', models.ForeignKey(to='webapp.Task')),
            ],
        ),
        migrations.AddField(
            model_name='task',
            name='students_set',
            field=models.ManyToManyField(to='webapp.Student', through='webapp.TickedTask'),
        ),
    ]
