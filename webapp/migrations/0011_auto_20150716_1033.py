# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0010_auto_20150715_1606'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='exam',
            options={'ordering': ['-date']},
        ),
        migrations.AddField(
            model_name='exam',
            name='number_of_tasks',
            field=models.IntegerField(default=1),
        ),
    ]
