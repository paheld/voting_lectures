# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0004_auto_20150415_1512'),
    ]

    operations = [
        migrations.AddField(
            model_name='sheet',
            name='last_sheet',
            field=models.ForeignKey(to='webapp.Sheet', blank=True, null=True),
        ),
    ]
