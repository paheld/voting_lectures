# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0009_remove_lecture_students'),
    ]

    operations = [
        migrations.CreateModel(
            name='Exam',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('date', models.DateField()),
                ('point_list', models.TextField(default='[]')),
                ('point_round', models.FloatField(default=0.5)),
                ('rating_max_points', models.FloatField(default=0)),
                ('rating_points_diff', models.FloatField(default=1)),
                ('lecture', models.ForeignKey(to='webapp.Lecture')),
            ],
        ),
        migrations.CreateModel(
            name='StudentExam',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('points', models.TextField(default='[]')),
                ('mark', models.FloatField(null=True, blank=True)),
                ('lsf_group', models.IntegerField(default=0)),
                ('exam', models.ForeignKey(to='webapp.Exam')),
                ('student', models.ForeignKey(to='webapp.Student')),
            ],
        ),
        migrations.AddField(
            model_name='exam',
            name='students',
            field=models.ManyToManyField(to='webapp.Student', blank=True, through='webapp.StudentExam'),
        ),
    ]
