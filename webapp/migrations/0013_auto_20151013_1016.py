# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0012_auto_20150716_1500'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='studentexam',
            options={'ordering': ['exam', 'student']},
        ),
        migrations.AddField(
            model_name='tickedtask',
            name='excused',
            field=models.BooleanField(default=False),
        ),
    ]
