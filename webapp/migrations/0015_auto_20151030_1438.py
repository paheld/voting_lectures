# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0014_auto_20151030_1435'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='lecture',
            options={'permissions': (('handle_lecture', 'Handle lecture'),), 'ordering': ['semester', 'name']},
        ),
    ]
