#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"


from webapp.models import *


class GeneralMiddleware(object):

    @staticmethod
    def process_request(request):
        if not "semester" in request.session or request.session["semester"] is None:
            try:
                request.session["semester"] = Semester.objects.first().id
            except:
                request.session["semester"] = None
