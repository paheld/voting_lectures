noten = [1, 1.3, 1.7, 2, 2.3, 2.7, 3, 3.3, 3.7, 4, 5]


###
  Handling Student List
###

student_list = Object()

add_student = (data) ->
  groups = []
  for field in $(".group-select")
    f = $(field)
    if f.prop("checked")
      groups.push(f.data("group"))
  post = {
    lecture_id: $(this).data("lecture"),
    student_id: data.data,
    groups: JSON.stringify(groups)
  }
  $.post("/actions/add_student_to_lecture/", post, (result) ->
    student_list[result.id] = result
    update_student_list()
  )
  $(this).val("")

toggle_group_assignment = () ->
  field = $(this)
  field.addClass("warning")
  if field.data("group")
    post = {
      student_id: field.data("student"),
      group_id: field.data("group"),
      remove: field.data("remove")
    }
    url = "/actions/update_group_assignment/"
  else
    post = {
      student_id: field.data("student"),
      exam_id: field.data("exam"),
      remove: field.data("remove")
    }
    url = "/actions/update_exam_assignment/"

  $.post(url, post, (result) ->
    student_list[result.id] = result
    update_student_list()
  )

remove_student = () ->
  field = $(this)
  post = {
    student_id: field.data("student"),
    lecture_id: window.lecture_id,
  }

  $.post("/actions/remove_student/", post, (result) ->
    console.log(result)
    delete student_list[result.id]
    update_student_list()
  )

submit_comment = () ->
  field = $(this)
  field.parent().addClass("has-warning").removeClass("has-error has-success")
  post = {
    student_id: field.data("student"),
    lecture_id: window.lecture_id,
    comment: field.val()
  }

  $.post("/actions/submit_comment/", post, (result) ->

    if result.comment == null
      result.comment = ""

    fields = $(".student-comment-"+result.student_id)
    fields.val(result.comment)
    fields.parent().addClass("has-success").removeClass("has-error has-warning")
    fields = $(".student-comment-text-"+result.student_id).text(result.comment)
    $("table").trigger("update")
  )
  $("#student-table").trigger("update")

get_mail_address = (student_id) ->
  if student_id == -1
    return window.user_mail
  return student_list[student_id].given_name+" "+student_list[student_id].surname+" <"+student_list[student_id].mail+">"

generate_mail_link = (to=-1, bcc=null, subject="["+window.lecture_symbol+"] ") ->
  link = "mailto:"
  to_value = get_mail_address(to)
  link = link + encodeURIComponent(to_value)
  link = link+"?"
  if bcc != null
    bcc_value = (encodeURIComponent(get_mail_address(x)) for x in bcc).join()
    if bcc_value
      link = link + "bcc="+bcc_value
      link = link + "&"
  link = link + "subject="+encodeURIComponent(subject)
  return link

update_student_list = (data=null) ->
  if data
    student_list = data
  table = $("#student_list").html("")
  for key, student of student_list
    row = $("<tr></tr>").appendTo(table)
    row.append($("<td></td>").append("<a class='btn btn-primary btn-xs' href='"+generate_mail_link(student.id)+"'><span class='glyphicon glyphicon-envelope'></span></a>"))
    row.append($("<td></td>").append(student.surname))
    row.append($("<td></td>").append(student.given_name))
    for g in window.group_list
      if g in student.group_ids
        field = $("<td class='text-center'></td>").append("<span class='glyphicon glyphicon-ok text-success'>")
        field.data("remove", true)
      else
        field = $("<td class='text-center'></td>").append("<span class='glyphicon glyphicon-minus text-danger'>")
        field.data("remove", false)
      field.data("student", key)
      field.data("group", g)
      field.click(toggle_group_assignment)
      field.appendTo(row)
    for e in window.exam_list
      if e in student.exam_ids
        field = $("<td class='text-center'></td>").append("<span class='glyphicon glyphicon-ok text-success'>")
        field.data("remove", true)
      else
        field = $("<td class='text-center'></td>").append("<span class='glyphicon glyphicon-minus text-danger'>")
        field.data("remove", false)
      field.data("student", key)
      field.data("exam", e)
      field.click(toggle_group_assignment)
      field.appendTo(row)

    if window.is_admin
      field = $("<td class='text-center'></td>").append("<span class='glyphicon glyphicon-remove text-danger'>")
      field.data("student", key)
      field.click(remove_student)
      field.appendTo(row)

    input = $("<input class='form-control student-comment-"+key+"'>")
    input.val(student.comment)
    input.data("student", key)
    input.change(submit_comment)
    field = $("<td class='text-center'></td>").append(input)
    field.appendTo(row)
  $("#student-table").trigger("update")

  for field in $(".mail-group")
    field = $(field)
    if field.data("type") == "all"
      ids = (student.pk for key, student of student_list)
    else if field.data("type") == "group"
      gid = parseInt(field.data("group"))
      ids = (student.pk for key, student of student_list when gid in student.group_ids)
    else if field.data("type") == "exam"
      eid = parseInt(field.data("exam"))
      ids = (student.pk for key, student of student_list when eid in student.exam_ids)
    else
      ids = []
    link = generate_mail_link(-1, ids)
    field.attr("href",link)

window.update_student_list = update_student_list


###
  Handling Voting List
###
update_tick_list = () ->
  for field in $(".tick-view")
    field = $(field)
    student = field.data("student")
    task = field.data("task")

    if window.tasks_done[student] == undefined
      continue

    if window.tasks_done[student][task] == undefined
      field.html('<span class="glyphicon glyphicon-minus text-danger"></span>')
      field.data("toggle", "x")
    else
      field.data("toggle", " ")
      if task in window.tasks_done[student].excused_list
        field.html('<span class="glyphicon glyphicon-check text-warning"></span>')
      else
        if window.tasks_done[student][task]
          field.html('<span class="glyphicon glyphicon-comment text-primary"></span>')
        else
          field.html('<span class="glyphicon glyphicon-ok text-success"></span>')

  for field in $(".tick-input")
    field = $(field)
    student = field.data("student")
    task = field.data("task")

    if window.tasks_done[student] == undefined
      continue

    if window.tasks_done[student][task] == undefined
      field.val(" ")
    else if task in window.tasks_done[student].excused_list
        field.val("e")
    else
      if window.tasks_done[student][task]
        field.val("o")
      else
        field.val("x")
  $(":focus").select()

  for field in $(".tick-counter")
    field = $(field)
    student = field.data("student")
    data_field = field.data("field")
    if window.tasks_done[student] != undefined
      field.html(window.tasks_done[student][data_field])

window.update_tick_list = update_tick_list

valid_inputs = ["", " ", "-", "1", "0", "n", "p", "v", "o", "x", "+", "j", "y", "k", "e", "i", "/", "*"]
submit_tick_input = (event) ->
  console.log(event.keyCode)
  field = $(this)
  field.val(field.val().toLowerCase())
  if field.val() not in valid_inputs
    field.parent().addClass("has-error").removeClass("has-warning has-success")
    this.select()
    return

  field.parent().addClass("has-warning").removeClass("has-error has-success")
  req = {
    task_id: field.data("task"),
    student_id: field.data("student")
    value: field.val()
  }
  if event.keyCode >= 48 or event.keyCode in [32, 39]
    $(".tick-input[tabindex="+(parseInt(field.attr("tabindex"))+1)+"]").select().focus()
  if event.keyCode == 38
    $(".tick-input[tabindex="+(parseInt(field.attr("tabindex"))-window.line_length)+"]").select().focus()
  if event.keyCode == 40
    $(".tick-input[tabindex="+(parseInt(field.attr("tabindex"))+window.line_length)+"]").select().focus()
  if event.keyCode == 37
    $(".tick-input[tabindex="+(parseInt(field.attr("tabindex"))-1)+"]").select().focus()

  $.post("/actions/update_tick_status/", req, (data)->
    window.tasks_done[data.student] = data.data
    field.parent().addClass("has-success").removeClass("has-warning has-error")
    update_tick_list()
  )

toggle_tick = (event) ->
  field = $(this)
  if event.button == 2
    value = "o"
  else
    value = field.data("toggle")
  req = {
    task_id: field.data("task"),
    student_id: field.data("student"),
    value: value
  }
  $.post("/actions/update_tick_status/", req, (data)->
    window.tasks_done[data.student] = data.data
    field.parent().addClass("has-success").removeClass("has-warning has-error")
    update_tick_list()
  )

present_tick = (event) ->
  console.log("dbl", event)
  toggle_tick(event, "o")

get_lecture_comments = (lecture_id) ->
  post = {lecture_id: lecture_id}
  $.post("/actions/get_lecture_comments/", post, (result) ->
    for key, val of result
      if val == null
        val = ""
      $(".student-comment-"+key).val(val)
      $(".student-comment-text-"+key).text(val)
    $("table").trigger("update")
  )
window.get_lecture_comments = get_lecture_comments


###
  Handling Exam Page
###

exam_update_total = () ->
  window.max_points = 0
  for i in [0...window.number_of_tasks]
    window.max_points += parseFloat($("#points-task-"+i).val())
  $("#total-points").text(window.max_points)
  $("#exam-result-max-points").prop("max", window.max_points)
  $("#exam-result-max-points-slider").prop("max", window.max_points)
  $("#exam-result-points-diff-abs").prop("max", window.max_points/10)


exam_update_student = (student_id, update_results=true) ->
  points = 0
  for i in [0...window.number_of_tasks]
    points += parseFloat($("#points-task-"+i+"-student-"+student_id).val())
  window.student_points[student_id] = points
  $("#total-points-student-"+student_id).text(points)
  if update_results
    update_exam_mark_table()


exam_update_all_students = () ->
  for field in $(".student-points-total")
    exam_update_student($(field).data("student"), false)
  update_exam_mark_table()


window.update_exam_page = () ->
  exam_update_total()
  exam_update_all_students()

window.submit_exam_input = (event) ->
  console.log(event, this)
  field = $(this)
  value = parseFloat(field.val().replace(",","."))
  if isNaN(value)
    field.parent().addClass("has-error").removeClass("has-warning has-success")
    this.select()
    return

  field.parent().addClass("has-warning").removeClass("has-error has-success")

  studend_id = field.data("student")

  req = {
    task: field.data("task"),
    student_id: field.data("student")
    value: value,
    exam_id: window.exam_id,
  }

  console.log(req)

  if studend_id != undefined
    $.post("/actions/update_exam_student_points/", req, (data)->
      field.val(data.value)
      field.parent().addClass("has-success").removeClass("has-warning has-error")
      exam_update_student(data.student_id)
    )
  else
    $.post("/actions/update_exam_points/", req, (data)->
      field.val(data.value)
      field.parent().addClass("has-success").removeClass("has-warning has-error")
      exam_update_total()
    )

keypress_exam_input = (event) ->
  event.preventDefault()
  field = $(this)
  if event.keyCode in [13, 32, 39]
    $(".tabindex[tabindex="+(parseInt(field.attr("tabindex"))+1)+"]").select().focus()
  if event.keyCode == 38
    $(".tabindex[tabindex="+(parseInt(field.attr("tabindex"))-window.number_of_tasks)+"]").select().focus()
  if event.keyCode == 40
    $(".tabindex[tabindex="+(parseInt(field.attr("tabindex"))+window.number_of_tasks)+"]").select().focus()
  if event.keyCode == 37
    $(".tabindex[tabindex="+(parseInt(field.attr("tabindex"))-1)+"]").select().focus()


update_exam_mark_table = () ->
  table = $("#rating-table").html("")

  diff = parseFloat($("#exam-result-points-diff-abs").val())
  points = parseFloat($("#exam-result-max-points").val())
  rounding = parseFloat($("#exam-round").val())

  students_done = []
  window.marks = Object()

  for n in noten
    row = $("<tr></tr>").appendTo(table)
    row.append("<td>"+n+"</td>")
    if n != 5
      points = points - diff
      points_round = Math.round(points/rounding) * rounding
      row.append("<td>&ge; "+points_round+"</td>")
    else
      row.append("<td>&lt; "+points_round+"</td>")
      points_round = 0

    cnt = 0
    for student_id, student_points of window.student_points
      if student_id in students_done
        continue
      if student_points >= points_round
        students_done.push(student_id)
        window.marks[student_id] = n
        cnt += 1
        field = $("#mark-student-"+student_id).text(n)
        if n == 1
          field.parent().parent().addClass("success").removeClass("danger")
        else if n == 5
          field.parent().parent().addClass("danger").removeClass("success")
        else
          field.parent().parent().removeClass("danger success")

    row.append("<td>"+cnt+"</td>")

  sum_x = 0
  sum_x2 = 0
  N = 0
  for s, p of window.student_points
    sum_x += p
    sum_x2 += p*p
    N += 1

  mean = sum_x / N
  varianz = sum_x2 / N - mean*mean
  std = Math.sqrt(varianz)

  for student_id, points of window.student_points
    diff = Math.round((points - mean)/std * 10) / 10
    if diff > 0
      txt = "+"
    else
      txt = ""
    txt += diff + "&thinsp;&sigma;"
    $("#diff-student-"+student_id).html(txt)

  req = {
    exam_id: window.exam_id,
    marks: JSON.stringify(window.marks)
  }
  if window.exam_id != undefined
    $.post("/actions/update_marks/", req)
    update_exam_graphs()


update_exam_config = () ->
  req = {
    exam_id: window.exam_id,
    point_round: $("#exam-round").val(),
    rating_max_points: $("#exam-result-max-points").val(),
    rating_points_diff_ratio: $("#exam-result-points-diff-ratio").val(),
  }
  $.post("/actions/update_exam_config/", req, () ->
    $("#exam-store-config-btn").removeClass("btn-warning btn-success").addClass("btn-default")
  )


prepare_exam_config_inputs = () ->

  $("#exam-round").change(() ->
    val = $(this).val()
    $("#exam-result-max-points-slider").prop("step", val)
    $("#exam-result-max-points").prop("step", val)
    $("#exam-result-points-diff-abs").prop("step", val)
    update_exam_mark_table()
    update_exam_config()
  )

  f = () ->
    $("#exam-result-max-points").val($(this).val())
    $("#exam-result-points-diff-abs").val(Math.round(parseFloat($(this).val())*parseFloat($("#exam-result-points-diff-ratio").val()))/100)
    update_exam_mark_table()
    update_exam_config()
  $("#exam-result-max-points-slider").change(f)
  $("#exam-result-max-points-slider").bind("input", f)

  f = () ->
    $("#exam-result-max-points-slider").val(parseFloat($(this).val()))
    $("#exam-result-points-diff-abs").val(Math.round(parseFloat($(this).val())*parseFloat($("#exam-result-points-diff-ratio").val()))/100)
    update_exam_mark_table()
    update_exam_config()
  $("#exam-result-max-points").change(f)
  $("#exam-result-max-points").bind("input", f)

  f = () ->
    val = $(this).val()
    mp = parseFloat($("#exam-result-max-points").val())
    $("#exam-result-points-diff-slider").val(val)
    $("#exam-result-points-diff-ratio").val(val)
    $("#exam-result-points-diff-abs").val(Math.round(parseFloat(val)*mp)/100)
    update_exam_mark_table()
    update_exam_config()
  $("#exam-result-points-diff-slider").change(f)
  $("#exam-result-points-diff-slider").bind("input",f)
  $("#exam-result-points-diff-ratio").change(f)
  $("#exam-result-points-diff-ratio").bind("input",f)

  f = () ->
    val = $(this).val()
    mp = parseFloat($("#exam-result-max-points").val())
    val = Math.round(val/mp*10000)/100
    $("#exam-result-points-diff-slider").val(val)
    $("#exam-result-points-diff-ratio").val(val)
    update_exam_mark_table()
    update_exam_config()
  $("#exam-result-points-diff-abs").change(f)
  $("#exam-result-points-diff-abs").bind("input",f)

  update_exam_mark_table()

update_exam_graphs = () ->
  notendist = []
  pass = 0
  fail = 0
  for n in noten
    cnt = 0
    for student_id, note of window.marks
      if note == n
        cnt += 1
    notendist.push(cnt)
    if n != 5
      pass += cnt
    else
      fail += cnt
  $("#mark-distribution-div").highcharts({
    chart: {
      type: "column",
    },
    title: {
      text: "Notenverteilung",
    },
    xAxis: {
      categories: noten,
      title: {
        text: null,
      }
    },
    yAxis: {
      title: {
        text: "Anzahl Studenten",
      }
    },
    legend: {
      enabled: false,
    }
    plotOptions: {
      column: {
        dataLabels: {
          enabled: true,
        }
      }
    },
    credits: {
      enabled: false,
    }
    series:
      [{
        name: "Anzahl"
        animation: false,
        data: (x for x in notendist),
      }]
  })

  $("#exam-pass-distribution-div").highcharts({
    chart: {
      type: "pie",
    },
    title: {
      text: null,
    },
    legend: {
      enabled: false,
    }
    plotOptions: {
      column: {
        dataLabels: {
          enabled: true,
        }
      }
    },
    credits: {
      enabled: false,
    },
    series:
      [{
        name: "Anzahl",
        colorByPoint: true,
        animation: false,
        data: [{
            name: "nicht bestandend",
            color: "#f45b5b",
            y: fail,
          }, {
            name: "bestandend",
            color: "#90ed7d",
            y: pass,
          }]
      }]
  })

  boxplot_data = []
  boxplot_raw_data = []
  for i in [0...window.number_of_tasks]
    data = []
    for student, mark of window.marks
      data.push(parseFloat($("#points-task-"+i+"-student-"+student).val()))
    data = data.sort((a,b) -> a-b)
    label = "Aufg. "+(i+1)
    boxplot_raw_data.push([label, data, parseFloat($("#points-task-"+i).val())])

  data = []
  for student, points of window.student_points
    data.push(points)
  data = data.sort((a,b) -> a-b)
  label = "gesamt"
  boxplot_raw_data.push([label, data, window.max_points])


  for raw in boxplot_raw_data
    data = raw[1]
    bp_min = data[0]
    bp_max = data[data.length-1]
    bp_q1 = data[parseInt(data.length/4)]
    bp_median = data[parseInt(data.length/2)]
    bp_q3 = data[parseInt(data.length*3/4)]

    boxplot_data.push([raw[0], bp_min, bp_q1, bp_median, bp_q3, bp_max])

  $("#points-abs-div").highcharts({
    chart: {
      type: "boxplot",
    },
    title: {
      text: "Absolute Punkteverteilung",
    },
    legend: {
      enabled: false,
    },
    yAxis: {
      min: 0,
      title: {
        text: "Punkte"
      }
    },
    xAxis: {
      categories: (x[0] for x in boxplot_data),
      title: {
        text: null,
      }
    }
    credits: {
      enabled: false,
    },
    series:
      [{
        name: "Punkte",
        animation: false,
        data: boxplot_data
      }]
  })

  boxplot_data = []
  for raw in boxplot_raw_data
    data = raw[1]
    bp_min = data[0] / raw[2] * 100
    bp_max = data[data.length-1] / raw[2]* 100
    bp_q1 = data[parseInt(data.length/4)] / raw[2]* 100
    bp_median = data[parseInt(data.length/2)] / raw[2]* 100
    bp_q3 = data[parseInt(data.length*3/4)] / raw[2]* 100

    boxplot_data.push([raw[0], bp_min, bp_q1, bp_median, bp_q3, bp_max])

  $("#points-rel-div").highcharts({
    chart: {
      type: "boxplot",
    },
    title: {
      text: "Relative Punkteverteilung",
    },
    legend: {
      enabled: false,
    }
    yAxis: {
      min: 0,
      title: {
        text: "Prozent"
      }
    },
    xAxis: {
      categories: (x[0] for x in boxplot_data),
      title: {
        text: null,
      }
    }
    credits: {
      enabled: false,
    },
    series:
      [{
        name: "Prozent",
        animation: false,
        data: boxplot_data
      }]
  })




###
  FRS - Stuff
###

fileSelectHandler = (e) ->
  #// cancel event and hover styling
  fileDragHover(e)
  #// fetch FileList object
  files = e.target.files || e.dataTransfer.files
  #// process all File objects
  parseFile(files[0])


fileDragHover = (e) ->
  e.stopPropagation()
  e.preventDefault()
  if e.type == "dragover"
    e.target.className = "hover"
  else
    e.target.className = ""


parseFile = (file) ->
  console.log(file, file.name, file.type, file.size)
  reader = new FileReader
  reader.onload = (e) ->
    fill_import_table(e.target.result)
  reader.readAsText(file, "latin1")


fill_import_table = (data) ->
  table = $("#import_list").html("")
  rows = data.split("\n")[1...]
  i = 0
  window.group_labels = Object()
  for row in rows
    elems = row.split(",")
    if elems.length < 6
      continue
    trow = $("<tr id='import-"+i+"' class='import-row'></tr>")
    table.append(trow)
    trow.data("number", i)
    trow.data("matrikel", elems[4])
    trow.data("email", elems[5])
    trow.data("pk", -1)
    trow.data("group", elems[0])
    window.group_labels[elems[0]] = -1
    trow.append($("<td>"+elems[0]+"</td>"))
    trow.append($("<td>"+elems[2]+"</td>"))
    trow.append($("<td>"+elems[1]+"</td>"))
    trow.append($("<td>"+elems[5]+"</td>"))
    trow.append($("<td class='given_name'>&nbsp;</td>"))
    trow.append($("<td class='surname'>&nbsp;</td>"))
    trow.append($("<td class='account'>&nbsp;</td>"))
    query = {
      given_name: elems[1],
      surname: elems[2],
      email: elems[5],
      row_id: i
    }
    input = $('<input type="text" class="change-import form-control" id="change-import'+i+'" placeholder="suchen..."
                               data-row="'+i+'">')
    input.autocomplete({
      serviceUrl: '/students/',
      onSelect: (data) ->
        data.row_id = $(this).data("row")
        fill_import_row(data)
    })

    trow.append($("<td></td>").append(input))
    $.getJSON("/student-query/", query, (data) ->
      fill_import_row(data)
    )
    i = i+1
  groups = $("#group_assignment").html("")
  for key, label of window.group_labels
    row = $("<div class='row'></div>").appendTo(groups)
    $("<div class='col-xs-6 col-sm-4 col-md-2'>"+key+"</div>").appendTo(row)
    field = $("<select class='group_label'></select>").appendTo($("<div class='col-xs-6 col-sm-4 col-md-2'></div>").appendTo(row))
    field.append("<option value='-1'>bitte w&auml;hlen</option>")
    field.data("label", key)
    for gid, gdesc of window.groups
      $("<option value='"+gid+"'>"+gdesc+"</option>").appendTo(field)


fill_import_row = (data) ->
  row = $("#import-"+data.row_id)
  $("#import-"+data.row_id+" .given_name").text(data.given_name)
  $("#import-"+data.row_id+" .surname").text(data.surname)
  $("#import-"+data.row_id+" .account").text(data.account)
  row.data("pk", parseInt(data.student_pk))


submit_frs_import = () ->
  students = Array()
  for g in $(".group_label")
    g = $(g)
    window.group_labels[g.data("label")] = parseInt(g.val())
  console.log(window.group_labels)
  for row in $(".import-row")
    row = $(row)
    students.push({
      pk: parseInt(row.data("pk")),
      mail: row.data("email"),
      matrikel: parseInt(row.data("matrikel")),
      group: window.group_labels[row.data("group")]
    })
  data = JSON.stringify(students)
  $("#import_data").val(data)
  return true


window.frs_init_loader = () ->
  fileselect = document.getElementById("fileselect")
  filedrag = document.getElementById("filedrag")

  # file select
  fileselect.addEventListener("change", fileSelectHandler, false)

	# file drop
  filedrag.addEventListener("dragover", fileDragHover, false)
  filedrag.addEventListener("dragleave", fileDragHover, false)
  filedrag.addEventListener("drop", fileSelectHandler, false)
  filedrag.style.display = "block"

  $("#import_button").click(submit_frs_import)

###
  Table Sorter Stuff
###

init_table_sorter = () ->

  $.tablesorter.addParser({
    id: "inputField",
    is: (s, table, cell) ->
      inputs = $(cell).find("input")
      if inputs.length != 1
        return false
      return true
    format: (s, table, cell) ->
      input = $($(cell).find("input")[0])
      return input.val()
    type: "text"
  })

  $.tablesorter.addParser({
    id: "glyphicon",
    is: (s, table, cell) ->
      if s != ""
        return false
      spans = $(cell).find(".glyphicon")
      if spans.length != 1
        return false
      return true
    format: (s, table, cell) ->
      input = $($(cell).find(".glyphicon")[0])
      return input.attr("class")
    type: "text"
  })

  $("table").tablesorter()

###
  Init Page
###
$(() ->
  $('#add_student_name').autocomplete({
    serviceUrl: '/students/',
    onSelect: add_student
  })
  $(".tick-input").change(submit_tick_input)
  $(".tick-input").keyup(submit_tick_input)
  $(".tick-input").click(() -> this.select())

  $(".tick-view").mouseup(toggle_tick)
  $(".tick-view").contextmenu(() -> false)

  $(".student-comment").change(submit_comment)

  $("#no-number").change(() ->
    if $(this).prop("checked")
      $("#task_number").val("")
    else
      $("#task_number").val($("#task_number").data("default"))
  )

  $("#task_number").change(() ->
    $("#no-number").prop("checked", $(this).val().length == 0)
  )

  prepare_exam_config_inputs()

  # Assign tabindex
  tabindex = 1
  $(".tabindex").each(() ->
    $(this).attr("tabindex", tabindex)
    tabindex = tabindex + 1
  )

  init_table_sorter()

  $("#create_lecture_link").click(() ->
    name = prompt("Name der Vorlesung")
    if name != null and name != ""
      $(this).attr("href", "/actions/create_lecture/"+encodeURIComponent(name)+"/")
  )

  $("#add_group_btn").click(() ->
    desc = prompt("Beschreibung der Gruppe")
    if desc != null and desc != ""
      $(this).attr("href", "/actions/create_group/"+window.lecture_id+"/"+encodeURIComponent(desc)+"/")
  )

  $("#add_exam_btn").click(() ->
    date = "---"
    while not (date.match(/^\d{4}-\d\d-\d\d$/) or date == null or date == "")
      date = prompt("Datum der Prüfung [yyyy-mm-dd]")
    if date != null and date != ""
      $(this).attr("href", "/actions/create_exam/"+window.lecture_id+"/"+encodeURIComponent(date)+"/")
  )

  $(".print-comment-switch").change((e) ->
    e.preventDefault()
    if $(this).prop("checked")
      $(".print-comment-switch").prop("checked", true)
      $(".printable-comment").removeClass("hidden-print")
    else
      $(".print-comment-switch").prop("checked", false)
      $(".printable-comment").addClass("hidden-print")
  )

  $(".disable-click").unbind()

  $(".exam-point-input").keyup(keypress_exam_input)
  $(".exam-point-input").click(() -> this.select())
  $(".exam-point-input").change(submit_exam_input)
)

###
  CSRF - Stuff
###
getCookie = (name) ->
    cookieValue = null;
    if (document.cookie && document.cookie != '')
        cookies = document.cookie.split(';')
        for cookie in cookies
            cookie = jQuery.trim(cookie)
            if (cookie.substring(0, name.length + 1) == (name + '='))
                return decodeURIComponent(cookie.substring(name.length + 1))

csrftoken = getCookie('csrftoken')

csrfSafeMethod = (method) ->
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method))

$.ajaxSetup({
  beforeSend: (xhr, settings) ->
    if (!csrfSafeMethod(settings.type) && !this.crossDomain)
      xhr.setRequestHeader("X-CSRFToken", csrftoken)
})