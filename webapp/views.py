from __future__ import print_function, division, unicode_literals, absolute_import, generators

from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponse, HttpResponseRedirect, HttpResponseForbidden, Http404
from django.core.urlresolvers import reverse
from django.utils.dateparse import parse_date
from django.contrib.auth.models import User
from django.contrib.auth.models import Group as SysGroup

from webapp.models import *
from webapp.excel import *
import subprocess
import os

import json

use_ldap3 = False
try:
    import ldap
except ImportError:
    import ldap3
    use_ldap3 = True

import ldap3
from django.contrib.auth.decorators import login_required


# Create your views here.
JSON_MIME = "application/json"

LDAP_SERVER = "ldap3.ovgu.de"
LDAP_SEARCHBASE = "dc=uni-magdeburg,dc=de"


def HttpResponseJSON(content, *args, **kwargs):
    return HttpResponse(json.dumps(content), *args, content_type=JSON_MIME, **kwargs)


@login_required
def index(request):
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    proc = subprocess.Popen(["/usr/bin/git", "log"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    report = proc.communicate()[0]
    return render(request, "index.html", {"gitlog":report})


@login_required
def get_students(request, lecture_id):
    lecture = get_object_or_404(Lecture, pk=lecture_id)
    if not lecture.can_see(request.user):
        return HttpResponseForbidden()
    groups = lecture.group_set.all()
    context = {
        "lecture": lecture,
        "groups": groups,
        "is_admin": lecture.can_admin(request.user),
        "is_exercise": lecture.can_exercise(request.user),
        "is_exam": lecture.can_exam(request.user)
    }
    if lecture.can_exercise(request.user):
        context["groups"] = groups
        context["json_group_list"] = json.dumps([g.pk for g in groups])
    else:
        context["groups"] = list()
        context["json_group_list"] = json.dumps([])

    if lecture.can_exam(request.user):
        context["exams"] = list(lecture.exam_set.all())
        context["json_exam_list"] = json.dumps([e.pk for e in lecture.exam_set.all()])
    else:
        context["exams"] = list()
        context["json_exam_list"] = json.dumps([])
    return render(request, "students.html", context)


@login_required
def student_list(request, lecture_id):
    lecture = get_object_or_404(Lecture, pk=lecture_id)
    if not lecture.can_see(request.user):
        return HttpResponseForbidden()
    students = {
        student.pk: student.get_jsonable(lecture) for student in lecture.participants.all()
    }
    return HttpResponseJSON(students)


def ldap_query(filter):
    suggestions = list()

    if use_ldap3:
        s = ldap3.Server(LDAP_SERVER)
        c = ldap3.Connection(s, auto_bind=True)

        c.search(LDAP_SEARCHBASE, str(filter),
                 attributes=["uid", "givenName", "sn", "mail"])
        response = c.response
        for r in response:
            attr = {key: r["attributes"][key][0]
                        if key in r["attributes"] and r["attributes"][key] else None
                    for key in ("uid", "givenName", "sn", "mail")
            }
            if attr["uid"]:
                student, created = Student.objects.get_or_create(uid=attr["uid"])
            else:
                student, created = Student.objects.get_or_create(given_name=attr["givenName"], surname=attr["sn"])
            if created:
                student.given_name = attr["givenName"]
                student.surname = attr["sn"]
                student.mail = attr["mail"]
                student.save()
            suggestions.append(student)
        c.unbind()

    else:
        con = ldap.initialize("ldap://ldap1.ovgu.de")
        con.simple_bind_s()

        base_dn = str("dc=uni-magdeburg,dc=de")
        attrs = [str("uid"), str("givenName"), str("sn"), str("mail")]
        x = con.search( base_dn, ldap.SCOPE_SUBTREE, filter, attrs)
        while True:
            try:
                r = con.result(x, all=0)[1][0][1]
                attr = {key: r[key][0]
                        if key in r and r[key] else None
                    for key in ("uid", "givenName", "sn", "mail")
                }
                if attr["uid"]:
                    try:
                        student, created = Student.objects.get_or_create(uid=attr["uid"])
                    except Student.MultipleObjectsReturned:
                        student = Student.objects.filter(uid=attr["uid"]).first()
                        created = False
                else:
                    try:
                        student, created = Student.objects.get_or_create(given_name=attr["givenName"], surname=attr["sn"])
                    except Student.MultipleObjectsReturned:
                        student = Student.objects.filter(given_name=attr["givenName"], surname=attr["sn"]). first()
                        created = False
                if created:
                    student.given_name = attr["givenName"]
                    student.surname = attr["sn"]
                    student.mail = attr["mail"]
                    student.save()
                suggestions.append(student)
            except ldap.SIZELIMIT_EXCEEDED:
                break
            except IndexError:
                break
        con.unbind()
    return suggestions


@login_required
def student_suggestions(request):
    query = request.GET["query"]
    query = query.replace(" ", "*")
    if "[" in query:
        query = query[:query.index("[")].strip()

    filter = str('(|(uid=*{}*)(cn=*{}*))'.format(query, query))

    suggestions = ldap_query(filter)

    suggestions = [{"value": "{} {} [{}]".format(student.given_name, student.surname, student.uid),
                    "data": student.pk,
                    "given_name": student.given_name,
                    "surname": student.surname,
                    "account": student.uid,
                    "student_pk": student.pk} for student in suggestions]

    return HttpResponseJSON({
        "query": query,
        "suggestions": suggestions
    })


@login_required
def query_user(request):
    row_id = request.GET.get("row_id")
    given_name = request.GET.get("given_name")
    surname = request.GET.get("surname")
    email = request.GET.get("email").lower()

    student = None

    # E-Mail bekannt
    try:
        student = Student.objects.get(mail=email)
    except (Student.DoesNotExist, Student.MultipleObjectsReturned):
        pass

    if not student:
        try:
            student = Student.objects.get(custom_mail=email)
        except (Student.DoesNotExist, Student.MultipleObjectsReturned):
            pass

    # Name bekannt?
    if not student:
        try:
            student = Student.objects.get(surname=surname, given_name=given_name)
        except (Student.DoesNotExist, Student.MultipleObjectsReturned) as e:
            pass
    if not student:
        try:
            student = Student.objects.get(surname=given_name, given_name=surname)
        except (Student.DoesNotExist, Student.MultipleObjectsReturned) as e:
            pass

    if not student:
        suggestions = ldap_query("(mail={})".format(email))
        if len(suggestions) == 1:
            student = suggestions[0]

    if not student:
        suggestions = ldap_query("(cn=*{}*{}*)".format(given_name, surname))
        if len(suggestions) == 1:
            student = suggestions[0]

    if not student:
        suggestions = ldap_query("(cn=*{}*{}*)".format(surname, given_name))
        if len(suggestions) == 1:
            student = suggestions[0]

    if student:
        return HttpResponseJSON({
        "given_name": student.given_name,
        "surname": student.surname,
        "account": student.uid,
        "row_id": row_id,
        "student_pk": student.pk,
    })

    #Beginn: fallback wenn ldap nicht funktioniert. by Heiner.
    noLdapUid = "noLdap-" + email
    student, created = Student.objects.get_or_create(uid=noLdapUid)
    if created:
        student.given_name = given_name
        student.surname = surname
        student.mail = email
        student.save()
    return HttpResponseJSON({
        "given_name": student.given_name,
        "surname": student.surname,
        "account": student.uid,
        "row_id": row_id,
        "student_pk": student.pk,
    })  
    #Ende: fallback wenn ldap nicht funktioniert. by Heiner.

    return HttpResponseJSON({
        "given_name": "-",
        "surname": "-",
        "account": "-",
        "row_id": row_id,
        "student_pl": -1,
    })


@login_required
def add_student_to_lecture(request):
    student = get_object_or_404(Student, pk=request.POST["student_id"])
    lecture = get_object_or_404(Lecture, pk=request.POST["lecture_id"])
    if not lecture.can_exercise(request.user) and not lecture.can_exam(request.user):
        return HttpResponseForbidden()

    sl_map, created = StudentLectureMap.objects.get_or_create(student=student, lecture=lecture)
    if created:
        sl_map.save()
    lecture.assign_related_object(sl_map)

    if "groups" in request.POST:
        group_ids = json.loads(request.POST["groups"])
        for gid in group_ids:
            group = get_object_or_404(Group, pk=gid)
            group.students.add(student)

    return HttpResponseJSON(student.get_jsonable(lecture))


@login_required
def add_student_to_group(request):
    student = get_object_or_404(Student, pk=request.POST["student_id"])
    group = get_object_or_404(Group, pk=request.POST["group_id"])
    if not group.lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    group.lecture.students.add(student)
    group.students.add(student)

    return HttpResponseJSON(student.get_jsonable(group.lecture))


@login_required
def remove_student(request):
    student = get_object_or_404(Student, pk=request.POST["student_id"])
    lecture = get_object_or_404(Lecture, pk=request.POST["lecture_id"])
    if not lecture.can_admin(request.user):
        return HttpResponseForbidden()

    StudentLectureMap.objects.filter(student=student, lecture=lecture).delete()
    for g in lecture.group_set.all():
        g.students.remove(student)

    return HttpResponseJSON({
        "status": "ok",
        "id": student.id
    })

@login_required()
def submit_comment(request):
    student = get_object_or_404(Student, pk=request.POST["student_id"])
    lecture = get_object_or_404(Lecture, pk=request.POST["lecture_id"])
    if not lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    comment = request.POST["comment"]

    StudentLectureMap.objects.filter(student=student, lecture=lecture).update(comment=comment)
    return HttpResponseJSON({
        "lecture_id": lecture.id,
        "student_id": student.id,
        "comment": comment,
    })


@login_required
def update_group_assignment(request):
    student = get_object_or_404(Student, pk=request.POST["student_id"])
    group = get_object_or_404(Group, pk=request.POST["group_id"])
    if not group.lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    if request.POST["remove"] == "true":
        group.students.remove(student)
    else:
        group.students.add(student)
    return HttpResponseJSON(student.get_jsonable(group.lecture))


@login_required
def update_exam_assignment(request):
    student = get_object_or_404(Student, pk=request.POST["student_id"])
    exam = get_object_or_404(Exam, pk=request.POST["exam_id"])
    if not exam.lecture.can_exam(request.user):
        return HttpResponseForbidden()

    if request.POST["remove"] == "true":
        StudentExam.objects.filter(exam=exam, student=student).delete()
    else:
        se, created = StudentExam.objects.get_or_create(exam=exam, student=student)
        if created:
            se.save()
        exam.lecture.assign_related_object(se)
    return HttpResponseJSON(student.get_jsonable(exam.lecture))


@login_required
def sheets(request, lecture_id):
    lecture = get_object_or_404(Lecture, pk=lecture_id)
    if not lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    sheets = lecture.sheet_set.all().prefetch_related("task_set")
    context = {
        "lecture": lecture,
        "sheets": sheets,
    }
    return render(request, "sheets.html", context)


@login_required
def create_sheet(request):
    lecture = get_object_or_404(Lecture, pk=request.POST["lecture_id"])
    if not lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    if "number" in request.POST:
        number = request.POST["number"]
    else:
        number = lecture.next_sheet_number()

    sheet = Sheet.objects.create(lecture=lecture, number=number, last_sheet=lecture.sheet_set.all().last())
    lecture.assign_related_object(sheet)

    return HttpResponseRedirect(reverse("sheets", args=(lecture.pk, )))


@login_required
def create_task(request):
    sheet = get_object_or_404(Sheet, pk=request.POST["sheet_id"])
    if not sheet.lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    number = None
    label = None
    optional = "optional" in request.POST

    if "number" in request.POST and request.POST["number"]:
        number = int(request.POST["number"])

    if "label" in request.POST and request.POST["label"]:
        label = request.POST["label"]

    task = Task.objects.create(sheet=sheet, number=number, label=label, optional=optional)
    sheet.lecture.assign_related_object(task)

    return HttpResponseRedirect(reverse("sheets", args=(sheet.lecture.pk, )))


def get_tick_results(student, sheet, max_tasks=None):
    tasks = dict()

    if max_tasks is None:
        max_tasks = Task.objects.filter(sheet_id__lte=sheet.id, sheet__lecture=sheet.lecture, optional=False).count()

    for t in TickedTask.objects.filter(student=student, task__sheet__lecture=sheet.lecture):
        tasks[t.task_id] = t.presented

    tasks["excused_list"] = [x[0] for x in
                             TickedTask.objects.filter(student=student, task__sheet_id__lte=sheet.id,
                                                       task__sheet__lecture=sheet.lecture,
                                                       excused=True).values_list("task__id")]
    tasks["ticked"] = TickedTask.objects.filter(student=student, task__sheet_id__lte=sheet.id,
                                                task__sheet__lecture=sheet.lecture, excused=False).count()
    tasks["presented"] = TickedTask.objects.filter(student=student, presented=True, task__sheet_id__lte=sheet.id,
                                                   task__sheet__lecture=sheet.lecture).count()
    tasks["excused"] = TickedTask.objects.filter(student=student, task__sheet_id__lte=sheet.id,
                                                 task__sheet__lecture=sheet.lecture, excused=True,
                                                 task__optional=False).count()
    try:
        tasks["ratio"] = round(100*tasks["ticked"]/(max_tasks-tasks["excused"])) if max_tasks else "-"
    except ZeroDivisionError:
        tasks["ratio"] = "-"
    tasks["max_tasks"] = max_tasks-tasks["excused"]
    tasks["ticked_addon"] = "/{}".format(tasks["max_tasks"]) if tasks["excused"] else ""

    return tasks


@login_required
def voting_list(request, sheet_id):
    sheet = get_object_or_404(Sheet, pk=sheet_id)
    lecture = sheet.lecture
    if not lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    students = list(sheet.lecture.participants.all())

    max_tasks = Task.objects.filter(sheet_id__lte=sheet_id, sheet__lecture=lecture, optional=False).count()

    tasks_done = dict()
    for s in students:
        tasks_done[s.pk] = get_tick_results(s, sheet, max_tasks)

    context = {
        "lecture": lecture,
        "sheet": sheet,
        "students": students,
        "tasks_done": tasks_done,
        "tasks_done_json": json.dumps(tasks_done),
        "max_tasks": max_tasks,
    }
    return render(request, "list.html", context)


@login_required
def update_tick_status(request):
    task = get_object_or_404(Task, pk=int(request.POST["task_id"]))
    student = get_object_or_404(Student, pk=int(request.POST["student_id"]))
    if not task.sheet.lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    value = request.POST["value"]

    TickedTask.objects.filter(task=task, student=student).delete()

    tt = None
    if value in ("p", "v", "o", "*"):
        tt = TickedTask.objects.create(task=task, student=student, presented=True)
    elif value in ("1", "x", "+", "j", "y"):
        tt = TickedTask.objects.create(task=task, student=student, presented=False)
    elif value in ("e", "k", "i", "/"):
        tt = TickedTask.objects.create(task=task, student=student, presented=False, excused=True)
    if tt:
        task.sheet.lecture.assign_related_object(tt)

    return HttpResponseJSON({
        "student": student.id,
        "data": get_tick_results(student, task.sheet)
    })


@login_required()
def get_lecture_comments(request):
    lecture = get_object_or_404(Lecture, pk=request.POST["lecture_id"])
    if not lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    result = {ls_map.student_id: ls_map.comment for ls_map in lecture.studentlecturemap_set.all()}
    return HttpResponseJSON(result)
[]

@login_required()
def exam(request, exam_id):
    exam = get_object_or_404(Exam, pk=exam_id)
    lecture = exam.lecture
    if not lecture.can_exam(request.user):
        return HttpResponseForbidden()

    if "number_of_tasks" in request.POST:
        exam.number_of_tasks = int(request.POST["number_of_tasks"])
        exam.save()

    point_list = json.loads(exam.point_list)
    while len(point_list) < exam.number_of_tasks:
        point_list.append(0)
        exam.point_list = json.dumps(point_list)
        exam.save()

    student_data = list(exam.studentexam_set.all().select_related("student"))
    for sd in student_data:
        sd.voted = TickedTask.objects.filter(student=sd.student, task__sheet__lecture=exam.lecture).count()
        sd.presented = TickedTask.objects.filter(student=sd.student, task__sheet__lecture=exam.lecture, presented=True).count()

    context = {
        "lecture": lecture,
        "exam": exam,
        "student_data": student_data,
    }
    return render(request, "exam.html", context)


@login_required()
def update_exam_student_points(request):
    exam = get_object_or_404(Exam, pk=int(request.POST["exam_id"]))
    if not exam.lecture.can_exam(request.user):
        return HttpResponseForbidden()

    stud_data = get_object_or_404(StudentExam, student_id=int(request.POST["student_id"]), exam=exam)
    task = int(request.POST["task"])
    points = float(request.POST["value"])

    point_list = json.loads(stud_data.points)
    while len(point_list) < task:
        point_list.append(0)
    point_list[task] = points

    stud_data.points = json.dumps(point_list)
    stud_data.save()

    return HttpResponseJSON({
        "exam_id": exam.id,
        "student_id": stud_data.student_id,
        "value": points,
        "task": task
    })


@login_required()
def update_exam_points(request):
    exam = get_object_or_404(Exam, pk=int(request.POST["exam_id"]))
    if not exam.lecture.can_exam(request.user):
        return HttpResponseForbidden()

    task = int(request.POST["task"])
    points = float(request.POST["value"])

    point_list = json.loads(exam.point_list)
    while len(point_list) < task:
        point_list.append(0)
    point_list[task] = points

    exam.point_list = json.dumps(point_list)
    exam.save()

    return HttpResponseJSON({
        "exam_id": exam.id,
        "value": points,
        "task": task
    })


@login_required()
def update_exam_config(request):
    exam = get_object_or_404(Exam, pk=int(request.POST["exam_id"]))
    if not exam.lecture.can_exam(request.user):
        return HttpResponseForbidden()

    if "point_round" in request.POST:
        exam.point_round = float(request.POST["point_round"])

    if "rating_max_points" in request.POST:
        exam.rating_max_points = float(request.POST["rating_max_points"])

    if "rating_points_diff_ratio" in request.POST:
        exam.rating_points_diff_ratio = float(request.POST["rating_points_diff_ratio"])

    exam.save()

    return HttpResponseJSON("ok")


@login_required()
def update_marks(request):
    exam = get_object_or_404(Exam, pk=int(request.POST.get("exam_id", -1)))
    if not exam.lecture.can_exam(request.user):
        return HttpResponseForbidden()

    marks = json.loads(request.POST["marks"])

    for student_id, mark in marks.items():
        StudentExam.objects.filter(exam=exam, student_id=int(student_id)).update(mark=mark)

    return HttpResponseJSON("ok")


@login_required()
def lecture_excel_export(request, lecture_id):
    lecture = get_object_or_404(Lecture, pk=int(lecture_id))
    if not lecture.can_admin(request.user):
        return HttpResponseForbidden()

    response = HttpResponse(get_lecture_export(lecture),
                            content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response["Content-Disposition"] = 'attachment; filename="{}.xlsx"'.format(lecture.get_short_name())
    return response


@login_required()
def change_semester(request, semester_id):
    request.session["semester"] = int(semester_id)
    return HttpResponseRedirect("/")


@login_required()
def frs_import_start(request, lecture_id):
    lecture = get_object_or_404(Lecture, pk=lecture_id)
    if not lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    groups = lecture.group_set.all()
    context = {
        "lecture": lecture,
        "groups": groups,
        "json_group_list": json.dumps([g.pk for g in groups]),
        "json_exam_list": json.dumps([e.pk for e in lecture.exam_set.all()])
    }
    return render(request, "import_frs.html", context)


@login_required()
def frs_import_process(request):
    lecture = get_object_or_404(Lecture, pk=int(request.POST.get("lecture_id")))
    if not lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    groups = lecture.group_set.all()
    data = json.loads(request.POST.get("data"))
    groups = {g.pk:g for g in groups}
    for student in data:
        pk = student.get("pk")
        gid = student.get("group")
        try:
            s = Student.objects.get(pk=pk)
        except Student.DoesNotExist:
            continue

        s.matrikel_number = int(student["matrikel"])
        mail = student["mail"].lower()
        if s.mail != mail:
            s.custom_mail = mail
        s.save()

        sl_map, created = StudentLectureMap.objects.get_or_create(student=s, lecture=lecture)
        if created:
            sl_map.save()

        if gid in groups:
            groups[gid].students.add(s)
    return HttpResponseRedirect(reverse("students", args=(lecture.pk,)))


@login_required()
def get_mail_list(request, lecture_id=None, group_id=None, exam_id=None):
    if group_id:
        group = get_object_or_404(Group, pk=int(group_id))
        if not group.lecture.can_exercise(request.user):
            return HttpResponseForbidden()

        students = group.students
    elif exam_id:
        exam = get_object_or_404(Exam, pk=int(exam_id))
        if not exam.lecture.can_exercise(request.user):
            return HttpResponseForbidden()

        students = exam.students
    else:
        lecture = get_object_or_404(Lecture, pk=int(lecture_id))
        if not lecture.can_exercise(request.user):
            return HttpResponseForbidden()

        students = lecture.participants
    txt = ""
    for s in students.all():
        txt += '{} {} <{}>\n'.format(s.given_name, s.surname, s.get_mail())
    response = HttpResponse(txt, content_type="text/plain")
    response["Content-Disposition"] = 'attachment; filename="maillist.txt"'
    return response


@login_required()
def check_permissions(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden()

    for l in Lecture.objects.all():
        obj_types = [
            StudentLectureMap,
            Group,
            Sheet,
            Exam
        ]

        for o in obj_types:
            for obj in o.objects.filter(lecture=l):
                l.assign_related_object(obj)

        for t in Task.objects.filter(sheet__lecture=l):
            l.assign_related_object(t)

        for tt in TickedTask.objects.filter(task__sheet__lecture=l):
            l.assign_related_object(tt)

        for se in StudentExam.objects.filter(exam__lecture=l):
            l.assign_related_object(se)

    return HttpResponse("OK")


@login_required()
def create_lecture(request, name):
    if not request.user.groups.filter(name="create_lectures").exists():
        return HttpResponseForbidden()

    symbol = "".join([x[0] for x in name.split(" ")])
    lecture = Lecture.objects.create(name=name, symbol=symbol, semester_id=request.session["semester"])
    lecture.save()
    groups = lecture.create_groups()
    for g in groups.values():
        request.user.groups.add(g)
    return HttpResponseRedirect(reverse("admin:webapp_lecture_change", args=(lecture.id,)))


@login_required()
def create_group(request, lecture_id, desc):
    lecture = get_object_or_404(Lecture, pk=int(lecture_id))
    if not lecture.can_exercise(request.user):
        return HttpResponseForbidden()

    group = Group.objects.create(lecture=lecture, desc=desc)
    lecture.assign_related_object(group)
    return HttpResponseRedirect(reverse("students", args=(lecture.id,)))


@login_required()
def create_exam(request, lecture_id, datestr):
    lecture = get_object_or_404(Lecture, pk=int(lecture_id))
    if not lecture.can_exam(request.user):
        return HttpResponseForbidden()

    exam = Exam.objects.create(lecture=lecture, date=parse_date(datestr))
    lecture.assign_related_object(exam)
    return HttpResponseRedirect(reverse("students", args=(lecture.id,)))


@login_required()
def lecture_rights(request, lecture_id):
    lecture = get_object_or_404(Lecture, pk=int(lecture_id))
    if not lecture.can_admin(request.user):
        return HttpResponseForbidden()
    users = list(User.objects.filter(is_staff=True))

    context = {
        "lecture": lecture,
        "users": users,
    }

    for subset in ["admin", "exercise", "exam"]:
        group_name = "lecture_{}_{}".format(lecture_id, subset)
        group = SysGroup.objects.get(name=group_name)
        context[subset] = set(group.user_set.all())

    return render(request, "rights.html", context)


@login_required()
def set_lecture_rights(request, lecture_id, user_id, subset, value):
    lecture = get_object_or_404(Lecture, pk=int(lecture_id))
    if not lecture.can_admin(request.user):
        return HttpResponseForbidden()

    user = get_object_or_404(User, pk=int(user_id))

    if subset not in ["admin", "exercise", "exam"]:
        raise Http404

    g = SysGroup.objects.get(name="lecture_{}_{}".format(lecture_id, subset))
    value = int(value)

    if subset != "admin" or user != request.user:
        if value:
            g.user_set.add(user)
        else:
            g.user_set.remove(user)

    return HttpResponseRedirect(reverse("lecture_rights", args=(lecture_id,)))
