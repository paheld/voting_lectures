from django.contrib import admin
from webapp.models import *
from guardian.admin import GuardedModelAdmin
from guardian.shortcuts import get_objects_for_user

# Register your models here.

class ExtendedGuardedModelAdmin(GuardedModelAdmin):
    def get_queryset(self, request):
        qs = super(ExtendedGuardedModelAdmin, self).get_queryset(request)
        # Check global permission
        if super(ExtendedGuardedModelAdmin, self).has_change_permission(request) \
            or (not self.list_editable and self.has_view_permission(request)):
                return qs
        # No global, filter by row-level permissions. also use view permission if the changelist is not editable
        #if self.list_editable:
        #    return get_objects_for_user(request.user, ["change_{}".format(self.opts.model_name)], qs)
        #else:
        #    return get_objects_for_user(request.user, ["change_{}".format(self.opts.model_name)], qs, any_perm=True)
        return get_objects_for_user(request.user, ["change_{}".format(self.opts.model_name)], qs,
                                    accept_global_perms=False)

    queryset = get_queryset

    def has_change_permission(self, request, obj=None):
        if super(ExtendedGuardedModelAdmin, self).has_change_permission(request, obj):
            return True
        if obj is None:
            # Here check global 'view' permission or if there is any changeable items
            return self.has_view_permission(request) or self.queryset(request).exists()
        else:
            # Row-level checking
            return request.user.has_perm("change_{}".format(self.opts.model_name), obj)
            #return request.user.has_perm(self.opts.get_change_permission(), obj)

    def get_view_permission(self):
        return 'change_%s' % self.opts.object_name.lower()

    def has_view_permission(self, request, obj=None):
        return request.user.has_perm(self.opts.app_label + '.' + self.get_view_permission(), obj)

    def has_delete_permission(self, request, obj=None):
        return super(ExtendedGuardedModelAdmin, self).has_delete_permission(request, obj) \
                or (obj is not None and request.user.has_perm("change_{}".format(self.opts.model_name), obj))
                #or (obj is not None and request.user.has_perm(self.opts.get_delete_permission(), obj))

admin.site.register(Lecture, ExtendedGuardedModelAdmin)
admin.site.register(Student)
admin.site.register(Group, ExtendedGuardedModelAdmin)
admin.site.register(Semester)
admin.site.register(Sheet, ExtendedGuardedModelAdmin)
admin.site.register(Task, ExtendedGuardedModelAdmin)
admin.site.register(TickedTask, ExtendedGuardedModelAdmin)
admin.site.register(Exam, ExtendedGuardedModelAdmin)
admin.site.register(StudentExam, ExtendedGuardedModelAdmin)
admin.site.register(StudentLectureMap, ExtendedGuardedModelAdmin)