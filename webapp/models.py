from __future__ import print_function, division, unicode_literals, absolute_import, generators

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from guardian.shortcuts import assign_perm
from django.contrib.auth.models import Group as SysGroup

import six
import json

# Create your models here.

class Student(models.Model):
    uid = models.CharField(max_length=64, null=True, unique=True)
    given_name = models.CharField(max_length=64, null=True)
    surname = models.CharField(max_length=64, null=True)
    mail = models.EmailField(null=True)
    custom_mail = models.EmailField(null=True)
    matrikel_number = models.IntegerField(null=True)

    class Meta:
        ordering = ["surname", "given_name", "uid"]

    def __str__(self):
        if six.PY2:
            return self.__unicode__().encode("ascii", "ignore")
        return self.__unicode__()

    def __unicode__(self):
        return "{} {} [{}]".format(self.given_name, self.surname, self.uid)

    def get_mail(self):
        if self.custom_mail:
            return self.custom_mail
        return self.mail

    def get_jsonable(self, lecture=None):
        groups = self.group_set.all()
        exams = self.exam_set.all()
        comment = None
        admission = None
        if lecture:
            groups = groups.filter(lecture=lecture)
            exams = exams.filter(lecture=lecture)
            try:
                sl_map = StudentLectureMap.objects.get(student=self, lecture=lecture)
                comment = sl_map.comment
            except StudentLectureMap.DoesNotExist:
                pass
        return {
            "pk": self.pk,
            "id": self.id,
            "uid": self.uid,
            "given_name": self.given_name,
            "surname": self.surname,
            "mail": self.custom_mail if self.custom_mail else self.mail,
            "group_ids": list(groups.values_list("pk", flat=True)),
            "exam_ids": list(exams.values_list("pk", flat=True)),
            "comment": comment,
        }


class Semester(models.Model):
    year = models.IntegerField()
    winter = models.BooleanField()

    def get_semester(self, short=False):
        if short:
            if self.winter:
                return "{}/{}".format(self.year, (self.year+1) % 100)
            return "{}".format(self.year)

        if self.winter:
            return "WiSe {}/{}".format(self.year, (self.year+1) % 100)
        return "SoSe {}".format(self.year)

    def __str__(self):
        return self.get_semester()

    class Meta:
        ordering = ["-year", "-winter"]


class StudentLectureMap(models.Model):
    student = models.ForeignKey(Student)
    lecture = models.ForeignKey("Lecture")
    comment = models.TextField(null=True, blank=True)
    admission = models.NullBooleanField(default=None)
    sub_group = "admin"


class Lecture(models.Model):
    semester = models.ForeignKey(Semester)
    english = models.BooleanField(default=False)
    symbol = models.CharField(max_length=8)
    name = models.CharField(max_length=64)
    sub_group = "admin"

    min_voting_ratio = models.FloatField(default=0.66)
    min_presentations = models.IntegerField(default=2)

    participants = models.ManyToManyField(Student, blank=True, through=StudentLectureMap)

    def get_full_name(self):
        return "{} - {}".format(self.name, self.semester.get_semester())

    def get_short_name(self):
        return "{} {}".format(self.symbol, self.semester.get_semester(short=True))

    def next_sheet_number(self):
        if not self.sheet_set.all().exists():
            return 1
        return max(self.sheet_set.all().values_list("number", flat=True)) + 1

    def next_task_number(self):
        query = Task.objects.filter(sheet__lecture_id=self.id).exclude(number=None)
        if not len(query):
            return 1
        return max(query.values_list("number", flat=True)) + 1

    def __str__(self):
        if six.PY2:
            return self.__unicode__().encode("ascii", "ignore")
        else:
            return self.__unicode__()

    def __unicode__(self):
        return self.get_full_name()

    def create_groups(self):
        groups = ["admin", "exercise", "exam"]
        group_objs = dict()
        for g in groups:
            group_name = "lecture_{}_{}".format(self.id, g)
            group, created = SysGroup.objects.get_or_create(name=group_name)

            if created:
                group.save()
            group_objs[g] = group

            if g == "admin":
                assign_perm("webapp.delete_lecture", group)
                assign_perm("webapp.delete_studentlecturemap", group)
                assign_perm("change_lecture", group, self)
                assign_perm("delete_lecture", group, self)
            elif g == "exercise" or g == "admin":
                assign_perm("webapp.delete_group", group)
                assign_perm("webapp.delete_sheet", group)
                assign_perm("webapp.delete_task", group)
                assign_perm("webapp.delete_tickedtask", group)
            elif g == "exam" or g == "admin":
                assign_perm("webapp.delete_exam", group)
                assign_perm("webapp.delete_studentexam", group)
        return group_objs

    def assign_related_object(self, obj):
        name = obj._meta.model_name
        group_name = "lecture_{}_{}".format(self.id, obj.sub_group)
        group, created = SysGroup.objects.get_or_create(name=group_name)

        if created:
            self.create_groups()
            group, _ = SysGroup.objects.get_or_create(name=group_name)

        assign_perm("change_{}".format(name), group, obj)
        assign_perm("delete_{}".format(name), group, obj)

    def can_see(self, user):
        return self.can_exercise(user) or self.can_exam(user) or self.can_admin(user)

    def can_exam(self, user):
        return self.has_lecture_perm(user, "exam")

    def can_exercise(self, user):
        return self.has_lecture_perm(user, "exercise")

    def can_admin(self, user):
        return self.has_lecture_perm(user, "admin")

    def has_lecture_perm(self, user, perm):
        return user.groups.filter(name="lecture_{}_{}".format(self.id, perm)).exists()

    class Meta:
        ordering = ["semester", "name"]


class Group(models.Model):
    lecture = models.ForeignKey(Lecture)
    desc = models.CharField(max_length=64)
    students = models.ManyToManyField(Student, blank=True)
    sub_group = "exercise"

    def __str__(self):
        return "{}  ---  {}".format(self.lecture.get_short_name(), self.desc)

    class Meta:
        ordering = ["lecture"]


class Sheet(models.Model):
    lecture = models.ForeignKey(Lecture)
    number = models.IntegerField(null=True)
    last_sheet = models.ForeignKey("self", null=True, blank=True)
    sub_group = "exercise"

    def __str__(self):
        return "{} - Blatt {}".format(self.lecture.get_short_name(), self.number)


class TickedTask(models.Model):
    student = models.ForeignKey(Student)
    task = models.ForeignKey("Task")
    presented = models.BooleanField(default=False)
    excused = models.BooleanField(default=False)
    sub_group = "exercise"


class Task(models.Model):
    sheet = models.ForeignKey(Sheet)
    number = models.IntegerField(null=True, blank=True)
    label = models.CharField(max_length=16, null=True, blank=True)
    optional = models.BooleanField(default=False)
    sub_group = "exercise"

    students_set = models.ManyToManyField(Student, through=TickedTask)

    def get_label(self):
        return self.label if self.label else self.number

    def __str__(self):
        return "{}: Aufgabe {}".format(self.sheet, self.get_label())


class StudentExam(models.Model):
    student = models.ForeignKey(Student)
    exam = models.ForeignKey("Exam")
    points = models.TextField(default="[]")
    mark = models.FloatField(blank=True, null=True)
    lsf_group = models.IntegerField(default=0)
    sub_group = "exam"

    def point_list_parsed(self):
        number_of_tasks = self.exam.number_of_tasks
        points_list = json.loads(self.points)
        while len(points_list) < number_of_tasks:
            points_list.append(0)
            self.points = json.dumps(points_list)
            self.save()
        return points_list[:number_of_tasks]

    class Meta:
        ordering = ["exam", "student"]


class Exam(models.Model):
    lecture = models.ForeignKey(Lecture)
    date = models.DateField()
    students = models.ManyToManyField(Student, blank=True, through=StudentExam)
    number_of_tasks = models.IntegerField(default=1)
    point_list = models.TextField(default="[]")
    point_round = models.FloatField(default=0.5)
    rating_max_points = models.FloatField(default=0)
    rating_points_diff_ratio = models.FloatField(default=5)
    sub_group = "exam"

    class Meta:
        ordering = ["-date"]

    def __str__(self):
        if six.PY2:
            return self.__unicode__().encode("ascii", "ignore")
        else:
            return self.__unicode__()

    def __unicode__(self):
        return "Exam [{}] - {}".format(self.date, self.lecture)

    def point_list_parsed(self):
        return json.loads(self.point_list)[:self.number_of_tasks]

    def rating_points_diff(self):
        return self.rating_max_points * self.rating_points_diff_ratio / 100


