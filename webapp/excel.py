#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"


import openpyxl
import openpyxl.compat
import openpyxl.cell
import openpyxl.styles

import tempfile
import os

from webapp.models import *

ALLIGNMENT_CENTERED = openpyxl.styles.Alignment(horizontal="center")
ALLIGNMENT_LEFT = openpyxl.styles.Alignment(horizontal="left")
FONT_BOLD = openpyxl.styles.Font(bold=True)
BORDER_BOTTOM_THICK = openpyxl.styles.Border(bottom=openpyxl.styles.Side(border_style="thick"))
BORDER_BOTTOM_THIN = openpyxl.styles.Border(bottom=openpyxl.styles.Side(border_style="thin"))
BORDER_LEFT_THIN = openpyxl.styles.Border(left=openpyxl.styles.Side(border_style="thin"))
BORDER_TOP_THIN = openpyxl.styles.Border(top=openpyxl.styles.Side(border_style="thin"))
BORDER_BOTTOM_THICK_LEFT_THIN = openpyxl.styles.Border(left=openpyxl.styles.Side(border_style="thin"),
                                                       bottom=openpyxl.styles.Side(border_style="thick"))
BORDER_TOP_THIN_LEFT_THIN = openpyxl.styles.Border(left=openpyxl.styles.Side(border_style="thin"),
                                                   top=openpyxl.styles.Side(border_style="thin"))

FONT_HEADER = openpyxl.styles.Font(bold=True, size=16)

def get_lecture_export(lecture):

    wb = openpyxl.Workbook()

    ws = wb.active
    ws.title = "Votierungen"

    ws['A1'] = lecture.__str__()
    c = ws['A1']
    c.font = FONT_HEADER
    ws['A3'] = "Name"
    c = ws['A3']
    c.alignment = ALLIGNMENT_LEFT
    c.border = BORDER_BOTTOM_THICK
    c.font = FONT_BOLD

    ws['B3'] = "Vorname"
    c = ws['B3']
    c.alignment = ALLIGNMENT_LEFT
    c.border = BORDER_BOTTOM_THICK
    c.font = FONT_BOLD

    ws.column_dimensions["A"].width = 25
    ws.column_dimensions["B"].width = 25

    sheets = list(lecture.sheet_set.all().order_by("number"))

    index = 3
    task_columns = dict()

    number_of_students = lecture.participants.all().count()

    for s in sheets:
        cnt = 0
        c = ws.cell(column=index, row=2, value="Blatt {}".format(s.number))
        c.alignment = ALLIGNMENT_CENTERED
        c.font = FONT_BOLD
        c.border = BORDER_LEFT_THIN

        for t in s.task_set.all():
            task_columns[t] = index
            c = ws.cell(column=index, row=3, value=t.get_label())
            c.alignment = ALLIGNMENT_CENTERED
            c.font = FONT_BOLD
            if cnt == 0:
                c.border = BORDER_BOTTOM_THICK_LEFT_THIN
                for i in range(number_of_students):
                    c = ws.cell(column=index, row=4+i, value="")
                    c.border = BORDER_LEFT_THIN
            else:
                c.border = BORDER_BOTTOM_THICK

            ws.column_dimensions[openpyxl.cell.get_column_letter(index)].width = 4
            index += 1
            cnt += 1
        ws.merge_cells(start_row=2, start_column=index-cnt, end_row=2, end_column=index-1)

    c = ws.cell(column=index, row=2, value="")
    c.border = BORDER_LEFT_THIN

    c = ws.cell(column=index, row=3, value="votiert")
    c.border = BORDER_BOTTOM_THICK_LEFT_THIN
    c.alignment = ALLIGNMENT_CENTERED
    c.font = FONT_BOLD

    c = ws.cell(column=index+1, row=3, value="vorgetragen")
    c.border = BORDER_BOTTOM_THICK
    c.alignment = ALLIGNMENT_CENTERED
    c.font = FONT_BOLD

    c = ws.cell(column=index+2, row=3, value="Kommentar")
    c.border = BORDER_BOTTOM_THICK
    c.alignment = ALLIGNMENT_CENTERED
    c.font = FONT_BOLD

    row = 4

    tick_area = "C{}:%s{}" % (openpyxl.cell.get_column_letter(index-1))

    for student in lecture.participants.all():
        ta = tick_area.format(row, row)
        c = ws.cell(column=index, row=row, value="=COUNTA({})".format(ta))
        c.alignment = ALLIGNMENT_CENTERED
        c.border = BORDER_LEFT_THIN

        c = ws.cell(column=index+1, row=row, value='=COUNTIF({}, "=O")'.format(ta))
        c.alignment = ALLIGNMENT_CENTERED

        ws.cell(column=index+2, row=row, value=StudentLectureMap.objects.get(student=student, lecture=lecture).comment)

        c = ws.cell(column=1, row=row, value=student.surname)
        c.alignment = ALLIGNMENT_LEFT
        c = ws.cell(column=2, row=row, value=student.given_name)
        c.alignment = ALLIGNMENT_LEFT
        for tt in TickedTask.objects.filter(student=student, task__sheet__lecture=lecture):
            col = task_columns[tt.task]
            if tt.presented:
                c = ws.cell(column=col, row=row, value="O")
            else:
                c = ws.cell(column=col, row=row, value="X")
            c.alignment = ALLIGNMENT_CENTERED


        row += 1

    for exam in lecture.exam_set.all():
        ws = wb.create_sheet(title="Prüfung vom {}".format(exam.date))
        ws['A1'] = "Name"
        c = ws['A1']
        c.alignment = ALLIGNMENT_LEFT
        c.font = FONT_BOLD
        c.border = BORDER_BOTTOM_THICK
        ws['B1'] = "Vorname"
        c = ws['B1']
        c.alignment = ALLIGNMENT_LEFT
        c.font = FONT_BOLD
        c.border = BORDER_BOTTOM_THICK

        ws.column_dimensions["A"].width = 25
        ws.column_dimensions["B"].width = 25

        for i in range(exam.number_of_tasks):
            c = ws.cell(column=i+3, row=1, value=i+1)
            c.alignment = ALLIGNMENT_CENTERED
            c.font = FONT_BOLD
            if i > 0:
                c.border = BORDER_BOTTOM_THICK
            else:
                c.border = BORDER_BOTTOM_THICK_LEFT_THIN

        c = ws.cell(column=3 + exam.number_of_tasks, row=1, value="Summe")
        c.alignment = ALLIGNMENT_CENTERED
        c.font = FONT_BOLD
        c.border = BORDER_BOTTOM_THICK_LEFT_THIN
        c = ws.cell(column=4 + exam.number_of_tasks, row=1, value="Note")
        c.alignment = ALLIGNMENT_CENTERED
        c.font = FONT_BOLD
        c.border = BORDER_BOTTOM_THICK
        row = 2
        for es in exam.studentexam_set.all():
            _ = ws.cell(column=1, row=row, value=es.student.surname)
            _ = ws.cell(column=2, row=row, value=es.student.given_name)

            points = json.loads(es.points)
            sum = 0
            for i in range(exam.number_of_tasks):
                try:
                    c = ws.cell(column=3+i, row=row, value=points[i])
                    sum += points[i]
                except IndexError:
                    c = ws.cell(column=3+i, row=row, value=0)
                c.alignment = ALLIGNMENT_CENTERED
                if i == 0:
                    c.border = BORDER_LEFT_THIN

            c = ws.cell(column=3 + exam.number_of_tasks, row=row, value=sum)
            c.alignment = ALLIGNMENT_CENTERED
            c.border = BORDER_LEFT_THIN

            c = ws.cell(column=4 + exam.number_of_tasks, row=row, value=es.mark)
            c.alignment = ALLIGNMENT_CENTERED

            row += 1

        c = ws.cell(column=2, row=row, value="")
        c.border = BORDER_TOP_THIN

        c = ws.cell(column=1, row=row, value="Mittelwert")
        c.font = FONT_BOLD
        c.border = BORDER_TOP_THIN
        c = ws.cell(column=1, row=row+1, value="Standardabweichung")
        c.font = FONT_BOLD
        for i in range(exam.number_of_tasks+2):
            column_letter = openpyxl.cell.get_column_letter(i+3)
            area = "{}{}:{}{}".format(column_letter, 2, column_letter, row-1)
            c1 = ws.cell(column=i+3, row=row, value="=AVERAGE({})".format(area))
            c2 = ws.cell(column=i+3, row=row+1, value="=STDEVP({})".format(area))
            if i%exam.number_of_tasks == 0:
                c1.border = BORDER_TOP_THIN_LEFT_THIN
                c2.border = BORDER_LEFT_THIN
            else:
                c1.border = BORDER_TOP_THIN

            for c in (c1, c2):
                c.alignment = ALLIGNMENT_CENTERED
                c.number_format = "0.00"

    _, filename = tempfile.mkstemp(prefix=".xlsx")
    wb.save(filename)

    with open(filename, "rb") as f:
        data = f.read()

    os.remove(filename)
    return data