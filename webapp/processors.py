#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

from webapp.models import *
from guardian.shortcuts import get_objects_for_user

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"


def filter_rights(request, queryset):
    return get_objects_for_user(request.user, "change_{}".format(queryset.model._meta.model_name), queryset)


def general_processor(request):
    context = dict()

    context["semesters"] = Semester.objects.all()
    if request.session["semester"]:
        context["current_semester"] = Semester.objects.get(pk=request.session["semester"])
        context["lectures"] = [l
                               for l in Lecture.objects.filter(semester_id=request.session["semester"])
                               if l.can_see(request.user)]
        for l in context["lectures"]:
            l.is_admin = l.can_admin(request.user)
            l.is_exercise = l.can_exercise(request.user)
            l.is_exam = l.can_exam(request.user)

    context["can_create_lectures"] = request.user.groups.filter(name="create_lectures").exists()

    return context